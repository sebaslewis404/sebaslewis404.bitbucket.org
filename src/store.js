import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { getOptions } from './helpers';

const initialState = {
  data: {},
  title: '',
  variant: {},
  images: [],
  selectedImage: {},
  colors: [],
  sizes: [],
  sizeSelected: {},
  sizeSelectorActive: false,
  cupSelectorActive: false,
};

const reducer = (state=initialState, action) => {

  if (action.type === 'LOAD_DATA') {
    const { colors, colorSelected, sizes, sizeSelected } = getOptions(action.data);
    return {
      ...state,
      title: action.data.product.title,
      variant: action.data.product.variants[0],
      images: action.data.product.images,
      selectedImage: action.data.product.images[0],
      colors,
      colorSelected,
      sizes,
      sizeSelected,
      content: action.data.product.body_html,
    } 
  } else if (action.type === 'SELECT_COLOR') {
    const color = state.colors.find(color => color.color === action.color.color);
    return {
      ...state,
      colorSelected: action.color.color,
      sizes: color.sizes,
      sizeSelected: color.sizes[0],
      cupSelected: undefined,
      stock: undefined,
    };
  } else if (action.type === 'CHANGE_BAND_SIZE') {
    return {
      ...state,
      sizeSelected: action.sizeSelected,
      sizeSelectorActive: false,
      cupSelected: undefined,
      stock: undefined,
    }
  } else if (action.type === 'TOGGLE_SIZE_SELECTOR') {
    return {
      ...state,
      sizeSelectorActive: !state.sizeSelectorActive,
      cupSelectorActive: false,
    }
  } else if (action.type === 'TOGGLE_CUP_SELECTOR') {
    return {
      ...state,
      cupSelectorActive: !state.cupSelectorActive,
      sizeSelectorActive: false,
    }
  } else if (action.type === 'CHANGE_CUP_SIZE') {
    return {
      ...state,
      cupSelected: action.cupSelected,
      stock: action.cupSelected.stock,
      cupSelectorActive: false,
    }
  } else if (action.type === 'ADD_TO_CART') {
    alert(`Added a ${state.title} - ${state.sizeSelected.value}${state.cupSelected.value} to the cart`)
  } else if (action.type === 'SELECT_IMAGE') {
    return {
      ...state,
      selectedImage: action.image,
    }
  }

  return state;
};

export default createStore(reducer, applyMiddleware(thunk));