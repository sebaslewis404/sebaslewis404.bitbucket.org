import axios from 'axios';

const loadData = () => {
  return dispatch => {
    axios.get('https://www.mocky.io/v2/5c6c3a92320000e83bbef971')
      .then(response => {
        dispatch({
          type: 'LOAD_DATA',
          data: response.data,
        });
      });
  }
}

const selectColor = (color) => {
  return { type: 'SELECT_COLOR', color };
}

const changeBandSize = (sizeSelected) => {
  return { type: 'CHANGE_BAND_SIZE', sizeSelected };
}

const changeCupSize = (cupSelected) => {
  return { type: 'CHANGE_CUP_SIZE', cupSelected };
}

const toggleSizeSelector = () => {
  return { type: 'TOGGLE_SIZE_SELECTOR' }
}

const toggleCupSelector = () => {
  return { type: 'TOGGLE_CUP_SELECTOR' }
}

const addToCart = () => {
  return { type: 'ADD_TO_CART' }
}

const selectImage = (image) => {
  return { type: 'SELECT_IMAGE', image };
}

export { 
  loadData,
  selectColor,
  changeBandSize,
  toggleSizeSelector,
  toggleCupSelector,
  changeCupSize,
  addToCart,
  selectImage,
};