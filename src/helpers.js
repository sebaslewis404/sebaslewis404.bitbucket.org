const getOptions = (data) => {
  const colors = [];
  data.product.variants.forEach(variant => {
    const color = variant.option1;
    const bandSize = variant.option2.slice(0,2);
    const cupSize = variant.option2.slice(2);

    const colorAlreadyInArray = colors.find(it => it.color === color);
    if (!colorAlreadyInArray) {
      colors.push({
        color,
        sizes: [{
          value: bandSize,
          cupSizes: [{value: cupSize, stock: variant.inventory_quantity}],
        }]
      });
    } else {
      const sizeAlreadyInArray = colorAlreadyInArray.sizes.find(it => it.value === bandSize);
      if (!sizeAlreadyInArray) {
        colorAlreadyInArray.sizes.push({
          value: bandSize,
          cupSizes: [{value: cupSize, stock: variant.inventory_quantity}],
        });
      } else {
        sizeAlreadyInArray.cupSizes.push({value: cupSize, stock: variant.inventory_quantity});
      }
    }
  });

  return {
    colors, 
    colorSelected: colors[0].color,
    sizes: colors[0].sizes,
    sizeSelected: colors[0].sizes[0],
  };
};

export { getOptions };