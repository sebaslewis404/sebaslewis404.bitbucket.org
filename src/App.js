import React, { Component } from 'react';
import './App.css';

import Header from './components/Header/Header';
import Carousel from './components/Carousel/Carousel';
import ColorSelector from './components/ColorSelector/ColorSelector';
import SizeSelector from './components/SizeSelector/SizeSelector';
import Button from './components/Button/Button';
import Stock from './components/Stock/Stock';
import Details from './components/Details/Details';
import Cart from './components/Cart/Cart';

class App extends Component {
  render() {
    return (
      <div className="App">
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <link href='//fonts.googleapis.com/css?family=Montserrat:thin,extra-light,light,100,200,300,400,500,600,700,800' rel='stylesheet' type='text/css'></link>
        <div className="flex grid">
          <Carousel />
          <Header />
          <Cart />
        </div>
        <Details />
      </div>
    );
  }
}

export default App;
