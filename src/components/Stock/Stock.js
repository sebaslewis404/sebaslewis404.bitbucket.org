import React from 'react';

import './Stock.css';
import { connect } from 'react-redux';

const Stock = ({ stock }) => (
  <div className="stock-container">
    <span className="stock">STOCK: <strong>{stock || '-'}</strong></span>
  </div>
);

const mapStateToProps = state => {
  return {
    stock: state.stock,
  };
};

export default connect(mapStateToProps)(Stock);