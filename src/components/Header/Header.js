import React from 'react';

import './Header.css';

import { connect } from 'react-redux';
import Reputation from '../Reputation';

const Header = ({title, variant, stars}) => (
  <div className="header">
    <h2 className="header__title">{title}</h2>
    <span className="header__price">${variant.price}</span>
    { stars && <Reputation stars={stars} /> }
  </div>
);

const mapStateToProps = state => {
  return {
    title: state.title,
    variant: state.variant,
    stars: state.stars,
  };
};


export default connect(mapStateToProps)(Header);