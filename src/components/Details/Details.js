import React from 'react'
import './Details.css';
import { connect } from 'react-redux';

const Details = ({content}) => (
  <div className="details">
    <h2 className="details__title">Details</h2>
    <hr />
    <div className="details__content" dangerouslySetInnerHTML={{__html: content}} />
  </div>
);

const mapStateToProps = state => {
  return {
    content: state.content,
  };
};

export default connect(mapStateToProps)(Details);