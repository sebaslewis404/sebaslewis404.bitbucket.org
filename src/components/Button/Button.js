import React from 'react';

import { connect } from 'react-redux';
import './Button.css';

import { addToCart } from '../../actionCreators';


const Button = ({addToCart}) => (
  <div className="button-container">
    <button className="button" onClick={addToCart}>Add to Bag</button>
  </div>
);

const mapStateToProps = state => {
  return {
    enabled: state.cartButtonEnabled,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    addToCart() {
      dispatch(addToCart());
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Button);