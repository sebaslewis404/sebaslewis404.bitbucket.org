import React from 'react';

const Reputation = ({stars}) => (
  <span>{stars}</span>
);

export default Reputation;