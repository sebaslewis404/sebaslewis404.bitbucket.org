import React from 'react';

import { connect } from 'react-redux';
import classnames from 'classnames';

import { selectColor } from '../../actionCreators';

import './ColorSelector.css';

const ColorSelector = ({colors, selectColor, colorSelected}) => (
  <div className="color-selector">
    <p className="color-selector__title">COLOR: <strong>{colorSelected}</strong></p>
    {colors.map(color => {
      const className = classnames(
        'color',
        `color--${color.color}`,
        {
          'color--selected': color.color === colorSelected,
        },
      );
      return (
        <div key={color.color} className={className} onClick={() => selectColor(color)} />
      )
    })}
  </div>
);

const mapStateToProps = state => {
  return {
    colors: state.colors,
    colorSelected: state.colorSelected,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    selectColor(color) {
      dispatch(selectColor(color));
    }
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(ColorSelector);