import React from 'react'

import ColorSelector from '../ColorSelector/ColorSelector';
import SizeSelector from '../SizeSelector/SizeSelector';
import Button from '../Button/Button';
import Stock from '../Stock/Stock';

import './Cart.css';

const Cart = () => (
  <div className="cart">
    <ColorSelector />
    <Stock />
    <SizeSelector />
    <Button />
  </div>
);

export default Cart;