import React from 'react';

import './SizeSelector.css'
import { connect } from 'react-redux';
import classnames from'classnames';

import { changeBandSize, toggleSizeSelector, toggleCupSelector, changeCupSize } from '../../actionCreators';

const Icon = ({isActive}) => {
  const classes = classnames('selector__icon', {
    'selector__icon--active': isActive
  })
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="9" viewBox="0 0 14 9" className={classes}>
      <path fill="#222" fillRule="evenodd" d="M8.091 7.084l.001.001-1.237 1.237h-.003L5.615 7.085l.001-.001L.385 1.852 1.622.615l5.232 5.231L12.085.615l1.237 1.237-5.23 5.232z"
      />
    </svg>
  )
}

const Selector = ({options, title, label = "Select", onClick, isActive, onClickOption}) => {
  const classes = classnames('selector__content', {
    'selector__content--active': isActive,
  });
  return (
    <div className="selector">
      <div className="selector__title">{title}</div>
      <button className="selector__link" onClick={onClick}>
        {label}
        <span className="selector__indicator">
          <Icon isActive={isActive} />
        </span>
      </button>
      <div className={classes}>
        { options.map(option => {
            const classes = classnames('selector__option', {
              'selector__option--active': option.value === label,
            })
            return (
              <div key={option.value} className={classes} onClick={() => onClickOption(option)}>{option.value}</div>
            )
          }
        )}
      </div>
    </div>
  )
};

const SizeSelector = ({changeCupSize, cupSelected, sizes, sizeSelected, changeBandSize, sizeSelectorActive, toggleSizeSelector, toggleCupSelector, cupSelectorActive}) => (
  <div className="size-selector">
    <Selector 
      options={sizes}
      label={sizeSelected.value}
      isActive={sizeSelectorActive}
      onClick={toggleSizeSelector}
      onClickOption={changeBandSize}
      title={"BAND SIZE"}
    />
    { sizeSelected.cupSizes && 
      <Selector
        options={sizeSelected.cupSizes}
        label={cupSelected && cupSelected.value} 
        isActive={cupSelectorActive} 
        onClick={toggleCupSelector}
        onClickOption={changeCupSize}
        title={"CUP SIZE"}
      />
    }
  </div>
);

const mapStateToProps = state => {
  return {
    sizes: state.sizes,
    sizeSelected: state.sizeSelected,
    sizeSelectorActive: state.sizeSelectorActive,
    cupSelectorActive: state.cupSelectorActive,
    cupSelected: state.cupSelected,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeBandSize(value) {
      dispatch(changeBandSize(value));
    },
    changeCupSize(value) {
      dispatch(changeCupSize(value));
    },
    toggleSizeSelector() {
      dispatch(toggleSizeSelector());
    },
    toggleCupSelector() {
      dispatch(toggleCupSelector())
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SizeSelector);