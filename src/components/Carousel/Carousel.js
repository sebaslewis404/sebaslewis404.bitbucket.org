import React from 'react';

import './Carousel.css';

import { connect } from 'react-redux';

import { selectImage } from '../../actionCreators';

import ReactSwipe from 'react-swipe';

const Carousel = ({images, selectedImage, selectImage}) => (
  <div className="carousel">
    <div className="carousel__slider">
      {images.map(image => (
        <img className="carousel__slider-image" onClick={() => selectImage(image)} src={`http://${image.src100}`}/>
      ))}
    </div>
    <img className="carousel__image" src={`http://${selectedImage.src1000}`}/>
  </div>
);

const mapStateToProps = state => {
  return {
    images: state.images,
    selectedImage: state.selectedImage,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    selectImage(image) {
      dispatch(selectImage(image));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Carousel);