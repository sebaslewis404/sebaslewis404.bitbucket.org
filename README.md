## ThirdLove app

This application was created using react-create-app.

## How to run the app

The app requires **node 10.x.x or later**

React Redux requires **React 16.4 or later.**


```
git clone git@bitbucket.org:sebaslewis404/sebaslewis404.bitbucket.org.git

cd sebaslewis404.bitbucket.org.git

npm install

npm start
```